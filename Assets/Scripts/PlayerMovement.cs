using Unity.Netcode;
using UnityEngine;

public class PlayerMovement : NetworkBehaviour
{
    private float horizontal;
    public NetworkVariable<float> speed = new NetworkVariable<float>(4f, NetworkVariableReadPermission.Everyone);
    public NetworkVariable<float> jumpingPower = new NetworkVariable<float>(16f, NetworkVariableReadPermission.Everyone);
    private NetworkVariable<bool> jumpChanged = new NetworkVariable<bool>(false, NetworkVariableReadPermission.Owner);
    public NetworkVariable<bool> isLookingRight = new NetworkVariable<bool>(false, NetworkVariableReadPermission.Everyone);
    private NetworkVariable<bool> speedChanged = new NetworkVariable<bool>(false, NetworkVariableReadPermission.Owner);

    [SerializeField] private Rigidbody2D rb;
    [SerializeField] private Transform groundCheck;
    [SerializeField] private LayerMask groundLayer;

    void Update()
    {
        if (!IsOwner) return;

        ChangedServerRpc(); 

        horizontal = Input.GetAxisRaw("Horizontal"); // get horizontal direction

        if (Input.GetButtonDown("Jump") && IsGrounded()) // if jumping on ground
        {
            rb.velocity = new Vector2(rb.velocity.x, jumpingPower.Value); // calculate new vector
        }

        if (Input.GetButtonUp("Jump") && rb.velocity.y > 0f) // jump based on space pressing
        {
            rb.velocity = new Vector2(rb.velocity.x, rb.velocity.y * 0.5f);
        }

        ChangeHorizontalServerRpc(horizontal);
    }

    private void FixedUpdate()
    {
        rb.velocity = new Vector2(horizontal * speed.Value, rb.velocity.y);
    }

    private bool IsGrounded()
    {
        return Physics2D.OverlapCircle(groundCheck.position, 0.3f, groundLayer);
    }

    [ServerRpc(RequireOwnership = false)]
    void ChangeHorizontalServerRpc(float horizontal, ServerRpcParams serverRpcParams = default)
    {
        if (horizontal == -1)
        {
            isLookingRight.Value = false;
        }
        if (horizontal == 1)
        {
            isLookingRight.Value = true;
        }
    }

    private void RevertSpeed()
    {
        speed.Value = 4f;
    }

    private void RevertJump()
    {
        jumpingPower.Value = 16f;
    }

    [ServerRpc(RequireOwnership = false)]
    void ChangedServerRpc(ServerRpcParams serverRpcParams = default)  //Check for changes (power ups picked up) and revert after certain amount
    {
        if (jumpingPower.Value != 16f)
        {
            jumpChanged.Value = true;
        }

        if (jumpChanged.Value)
        {
            Invoke("RevertJump", 4f);
            jumpChanged.Value = false;
        }

        if (speed.Value != 4f)
        {
            speedChanged.Value = true;
        }

        if (speedChanged.Value)
        {
            Invoke("RevertSpeed", 4f);
            speedChanged.Value = false;
        }
    }
}

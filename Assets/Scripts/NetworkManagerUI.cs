using TMPro;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.UI;

public class NetworkManagerUI : NetworkBehaviour
{
    [SerializeField] private Button serverButton;
    [SerializeField] private Button clientButton;
    [SerializeField] private Button disconnectButton;

    [SerializeField] private TextMeshProUGUI playersCountText;
    [SerializeField] public TMP_InputField UserNameTextField;

    [SerializeField] private GameObject powerUpSpawner;

    private NetworkVariable<int> playersCount = new NetworkVariable<int>(0, NetworkVariableReadPermission.Everyone);

    private void Awake()
    {
        disconnectButton.gameObject.SetActive(false); // initial set up for dc button

        serverButton.onClick.AddListener(() => // start server button
        {
            NetworkManager.Singleton.StartServer();
            SpawnSpawener();
            DisableButtons();
        });

        clientButton.onClick.AddListener(() => // start client button
        {
            NetworkManager.Singleton.StartClient();

            DisableButtons();
        });

        disconnectButton.onClick.AddListener(() => // disconnect button
        {
            NetworkManager.Singleton.Shutdown();
            EnableButtons();
        });
    }

    private void SpawnSpawener() // set up power up spawner
    {
        if (!IsServer) return;

        GameObject spawnPowerUpSpawner;
        spawnPowerUpSpawner = Instantiate(powerUpSpawner);
        spawnPowerUpSpawner.GetComponent<NetworkObject>().Spawn();
    }

    private void DisableButtons() // set buttons for in-game
    {
        serverButton.gameObject.SetActive(false);
        clientButton.gameObject.SetActive(false);
        disconnectButton.gameObject.SetActive(true);
        UserNameTextField.gameObject.SetActive(false);
    }

    private void EnableButtons() // set buttons for out of game
    {
        serverButton.gameObject.SetActive(true);
        clientButton.gameObject.SetActive(true);
        disconnectButton.gameObject.SetActive(false);
    }

    private void OnClientConnectedCallback()
    {
        playersCountText.text = "Players: " + playersCount.Value.ToString(); // update number of players in-game
        ChangePlayerCountServerRPC();
    }

    private void Update()
    {
        OnClientConnectedCallback();
    }

    [ServerRpc(RequireOwnership = false)]
    void ChangePlayerCountServerRPC(ServerRpcParams serverRpcParams = default)
    {
        playersCount.Value = NetworkManager.Singleton.ConnectedClients.Count;
    }
}

using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;

public class PowerUpSpawner : NetworkBehaviour
{
    [SerializeField] private GameObject healthPowerUp;
    [SerializeField] private GameObject speedPowerUp;
    [SerializeField] private GameObject jumpPowerUp;

    private List<GameObject> powerUps;

    void Start()
    {
        powerUps = new List<GameObject>();
        powerUps.Add(healthPowerUp);
        powerUps.Add(speedPowerUp);
        powerUps.Add(jumpPowerUp);

        Invoke("Spawn", 1f);
    }

    private void SetNewPosition()
    {
        Vector2 newPosition = new Vector2(Random.Range(-7.5f, 7.5f), 3.5f);
        transform.position = newPosition;
    }

    public void SpawnPowerUp()
    {
        GameObject spawnPowerUp = Instantiate(powerUps[Random.Range(0, 3)], transform);
        spawnPowerUp.GetComponent<NetworkObject>().Spawn();
    }

    public void Spawn()
    {
        SetNewPosition();
        SpawnPowerUp();

        Invoke("Spawn", Random.Range(5f, 10f));
    }
}

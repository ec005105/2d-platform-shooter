using Unity.Netcode;
using UnityEngine;

public class HealthPowerUp : NetworkBehaviour
{
    private void Start()
    {
        Invoke("SelfDestruct", 15f);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!IsServer) return;

        if (collision.gameObject.TryGetComponent<PlayerHealth>(out PlayerHealth playerHealth))
        {
            var id = collision.gameObject.GetComponent<NetworkObject>().OwnerClientId;

            var target = NetworkManager.ConnectedClients[id];

            target.PlayerObject.GetComponent<PlayerHealth>().playerHealth.Value += 1;

        }

        Destroy(gameObject);
    }

    private void SelfDestruct()
    {
        Destroy(gameObject);
    }
}

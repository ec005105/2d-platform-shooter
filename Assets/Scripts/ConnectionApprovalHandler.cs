using UnityEngine;
using Unity.Netcode;
using TMPro;

public class ConnectionApprovalHandler : MonoBehaviour
{
    private const int MaxPlayers = 4; // Max number of players that can connect to server

    private void Start()
    {
        NetworkManager.Singleton.ConnectionApprovalCallback = ApprovalCheck; // Set approval check
    }

    private void ApprovalCheck(NetworkManager.ConnectionApprovalRequest request, NetworkManager.ConnectionApprovalResponse response)
    {
        response.Approved = true; // Set connection approved at first

        if (NetworkManager.Singleton.ConnectedClients.Count >= MaxPlayers) // If more than max players in game
        {
            response.Approved = false; // Un-approve connection
            response.Reason = "Server full"; // Set reason of "Server full"
        }
       
        response.CreatePlayerObject = true; // If server is not full, create player object prefab
        response.PlayerPrefabHash = null; // don't specify hash for prefab
        response.Pending = false;
    }
}
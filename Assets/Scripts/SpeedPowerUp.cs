using Unity.Netcode;
using UnityEngine;

public class SpeedPowerUp : NetworkBehaviour
{
    private void Start()
    {
        Invoke("SelfDestruct", 15f); // destroy powerup after some time if not collected
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!IsServer) return;

        if (collision.gameObject.TryGetComponent<PlayerMovement>(out PlayerMovement playerMovment)) // when collided with a player
        {
            var id = collision.gameObject.GetComponent<NetworkObject>().OwnerClientId; // get client id

            var target = NetworkManager.ConnectedClients[id]; // get network client

            target.PlayerObject.GetComponent<PlayerMovement>().speed.Value = 8f; // modify player object network variables from network client
        }

        Destroy(gameObject); // destroy self
    }

    private void SelfDestruct()
    {
        Destroy(gameObject);
    }
}

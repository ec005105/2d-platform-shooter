using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;

public class PlayerTracker : NetworkBehaviour
{
    public NetworkList<int> player = new NetworkList<int>(null, NetworkVariableReadPermission.Owner, NetworkVariableWritePermission.Server);
    public NetworkList<int> health = new NetworkList<int>(null, NetworkVariableReadPermission.Owner, NetworkVariableWritePermission.Server);

    void Update()
    {
        foreach (int player in player)
        {
            Debug.Log(player);
        }
        foreach (int health in health)
        {
            Debug.Log(health);
        }
    }
}

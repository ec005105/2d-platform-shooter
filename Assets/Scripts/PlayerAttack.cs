using Unity.Netcode;
using UnityEngine;
using System.Threading.Tasks;

public class PlayerAttack : NetworkBehaviour
{
    [SerializeField] private GameObject fireball;
    [SerializeField] private Transform leftShoot;
    [SerializeField] private Transform rightShoot;

    public NetworkVariable<bool> attackAgain = new NetworkVariable<bool>(true, NetworkVariableReadPermission.Everyone, NetworkVariableWritePermission.Server);

    void Update()
    {
        if (!IsOwner) return;

        if (Input.GetKeyDown("j"))
        {
            AttackServerRpc();
        }
    }

    [ServerRpc(RequireOwnership = false)]
    void AttackServerRpc(ServerRpcParams serverRpcParams = default)
    {
        if (attackAgain.Value)
        {
            bool isLookingRight = GetComponent<PlayerMovement>().isLookingRight.Value;
            GameObject spawnFireball;

            if (isLookingRight == false)
            {
                spawnFireball = Instantiate(fireball, leftShoot);
                spawnFireball.GetComponent<FireballMovement>().SetHorizontal(-1);
            }
            else
            {
                spawnFireball = Instantiate(fireball, rightShoot);
                spawnFireball.GetComponent<FireballMovement>().SetHorizontal(1);
            }

            spawnFireball.GetComponent<NetworkObject>().Spawn();

            attackAgain.Value = false;
            Debug.Log(attackAgain.Value);

            async void AttackAgain()
            {
                await Task.Delay(2000);
                attackAgain.Value = true;
            }

            AttackAgain();
        }
    }
}

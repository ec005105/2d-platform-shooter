using TMPro;
using Unity.Netcode;
using UnityEngine;

public class PlayerHealth : NetworkBehaviour
{
    public NetworkVariable<int> playerHealth = new NetworkVariable<int>(3, NetworkVariableReadPermission.Everyone, NetworkVariableWritePermission.Server);
    public NetworkVariable<int> playerId = new NetworkVariable<int>(0, NetworkVariableReadPermission.Everyone, NetworkVariableWritePermission.Server);
    public GameObject record;

    [SerializeField] private TextMeshProUGUI healthText;

    private void Awake()
    {
        Invoke("SaveHealth", 10f);
        Invoke("LoadHealth", 0.5f);
    }

    private void Update()
    {
        healthText.text = playerHealth.Value.ToString();

        if (playerHealth.Value == 0)
        {
            gameObject.SetActive(false);
            Invoke("Respawn", 5f);
            ResetHealthServerRpc();
        }

        if (playerId.Value == 0)
        {
            GameObject userInput = GameObject.Find("NetworkManagerUI");
            SetPlayerIdServerRpc(int.Parse(userInput.GetComponent<NetworkManagerUI>().UserNameTextField.text));
            Debug.Log(playerId.Value);
        }
    }

    public void SaveHealth()
    {
        SaveHealthServerRpc(playerId.Value, playerHealth.Value);
        Invoke("SaveHealth", 5f);
    }

    public void LoadHealth()
    {
        LoadHealthServerRpc(playerId.Value);
    }

    private void Respawn()
    {
        gameObject.SetActive(true);
    }

    [ServerRpc(RequireOwnership = false)]
    void ResetHealthServerRpc(ServerRpcParams serverRpcParams = default)
    {
        playerHealth.Value = 3;
    }

    [ServerRpc(RequireOwnership = false)]
    void SaveHealthServerRpc(int player, int health, ServerRpcParams serverRpcParams = default)
    {
        record = GameObject.Find("PlayerTracker");
        
        if (record.GetComponent<PlayerTracker>().player.IndexOf(player) == -1)
        {
            record.GetComponent<PlayerTracker>().player.Add(player);
            record.GetComponent<PlayerTracker>().health.Add(health);
        }
        else
        {
            record.GetComponent<PlayerTracker>().health[record.GetComponent<PlayerTracker>().player.IndexOf(player)] = health;
        }
    }

    [ServerRpc(RequireOwnership = false)]
    void SetPlayerIdServerRpc(int id, ServerRpcParams serverRpcParams = default)
    {
        playerId.Value = id;
    }

    [ServerRpc(RequireOwnership = false)]
    void LoadHealthServerRpc(int id, ServerRpcParams serverRpcParams = default)
    {
        record = GameObject.Find("PlayerTracker");
        int index = record.GetComponent<PlayerTracker>().player.IndexOf(id);
        if (index != -1)
        {
            playerHealth.Value = record.GetComponent<PlayerTracker>().health[index];
        }
    }

}
